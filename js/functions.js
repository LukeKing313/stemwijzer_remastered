//defining the elements to put the data in or get data from
var subjectTitle = document.getElementById("subject_title");
var subjectStatement = document.getElementById("subject_statement");
var valueCheckbox = document.getElementById("double_count");
var resultsList = document.getElementById("results_list");
var error = document.getElementById("error");

//defining the elements for the party filter later on
var allSeats = document.getElementById("all_seats");
var secularSeats = document.getElementById("secular_seats");
var xSeats = document.getElementById("x_seats");
var xSeatsInput = document.getElementById("x_seats_input");

//defining the views
var startContainer = document.getElementById("start_container");
var formContainer = document.getElementById("form_container");
var resultsFilterContainer = document.getElementById("results_filter_container");
var resultsContainer = document.getElementById("results_container");

//creating variables to keep track of data like answers
var filterPass = false;
var subjectPosition = 0;
var displayPosition;
var answers = [];
var partyScore = [];
var doubleCount = [];
var maxPoints = subjects.length * 2;
for (var s = 0; s < parties.length; s++) {
    partyScore[s] = {"name": parties[s].name, "points": 0};
}
console.log(partyScore);

//putting the first subject into the elements
subjectIntoElement(); 

//creating a function to switch between 3 views
function viewSwitch(buttonPress){
	switch(buttonPress){
		case "start":
			startContainer.classList.toggle("displayContainer");
			formContainer.classList.toggle("displayContainer");
		break;

		case "formback":
			formContainer.classList.toggle("displayContainer");
			startContainer.classList.toggle("displayContainer");
		break;

		case "formforward":
			formContainer.classList.toggle("displayContainer")
			resultsFilterContainer.classList.toggle("displayContainer")
		break;

		case "results":
			resultsFilterContainer.classList.toggle("displayContainer");
			resultsContainer.classList.toggle("displayContainer");
		break;

		case "redo":
			resultsContainer.classList.toggle("displayContainer");
			startContainer.classList.toggle("displayContainer");
		break;
	}
}


//creating a function that inserts the answers into the array of answers
function answerIntoArray(answer){
	answers[subjectPosition] = answer;
	changeSubjectPosition("forward")
}

//creating a function to move to the next or previous subject
function changeSubjectPosition(direction){
	switch(direction){
		case "back":
			if (subjectPosition != 0){
				subjectPosition--;
				valueCheckbox.checked = false;

			} else {
				viewSwitch("formback");
			}
			break;
		case "forward":
			if (subjectPosition != subjects.length - 1){
				if (valueCheckbox.checked) {
					doubleCount[subjectPosition] = true;
					valueCheckbox.checked = false;
				}else{
					doubleCount[subjectPosition] = false;
				}
				subjectPosition++;
				console.log(doubleCount);
			} else {
				viewSwitch("formforward");
				console.log(answers);
				calculateResults();
			}
			break;
	}
	subjectIntoElement();
}

//creating a function that puts the current subject into the elements
function subjectIntoElement(){
	displayPosition = subjectPosition + 1;
	subjectTitle.innerHTML = displayPosition + ". " + subjects[subjectPosition].title;
	subjectStatement.innerHTML = subjects[subjectPosition].statement;
}

//creating a function to calculate the results and sort them in descending order
function calculateResults() {
    for (var i = 0; i < answers.length; i++) {
    	for (var k = 0; k < partyScore.length; k++) {
    		if (answers[i] == subjects[i].parties[k].position) {
    			for (var f = 0; f < partyScore.length; f++) {
    				if (subjects[i].parties[k].name == partyScore[f].name) {
    					partyScore[f].points++;
    					if (doubleCount[i]) {
    							partyScore[f].points++;
    					}
    				}
    			}
    		}
    	}
    }
	// sorteerd de array op punten 
	partyScore.sort(function(a,b) {
	    return b["points"]-a["points"]
	});
	console.log(partyScore);
}

//a function to filter out the results to only show what the user wants to see
function filterResults() {
	if (!secularSeats.checked && !xSeats.checked) {

	}
	if (xSeats.checked){
		if (!xSeatsInput.value) {
			error.style.display = "block";
			filterPass = false;
		} else if (xSeatsInput.value) {
			error.style.display = "none";
			filterPass = true;
			xSeatsFilter();
		}
	} else {
		filterPass = true;
	}
	if (secularSeats.checked) {
		secularFilter();
	}
	if (filterPass) {
		showResults();
	}
	console.log(partyScore);
}

//a function to show the results on the result page
function showResults() {
	viewSwitch("results");
	for (var i = 0; i < partyScore.length; i++) {
			var node = document.createElement("li");
			var nameNode = document.createElement("h4");
			var numberNode = document.createElement("p");
			node.classList.add("results_party");
			numberNode.innerHTML = convertToPercentage(partyScore[i].points) + "%";
			nameNode.innerHTML = partyScore[i].name;
			node.appendChild(nameNode);
			node.appendChild(numberNode);
			resultsList.appendChild(node);
	}
}

//a function to filter the parties on a user input minimum amount of members
function xSeatsFilter(){
	for (var i = 0; i < partyScore.length; i++) {
		for (var x = 0; x < parties.length; x++) {
			if(partyScore[i] != undefined) {
				if (parties[x].name == partyScore[i].name) {
					if (parties[x].size < xSeatsInput.value) {
						console.log(partyScore[i].name , " - ", parties[x].name, " - ", parties[x].size);
						partyScore.splice(i,1);
						i--;
					}
				}
			}
		}
	}
}

//a function to only show parties which are secular
function secularFilter(){
	for (var i = 0; i < partyScore.length; i++) {
		for (var x = 0; x < parties.length; x++) {
			if (parties[x].name == partyScore[i].name) {
				if (!parties[x].secular) {
					partyScore.splice(i,1);
				}
			}
		}
	}
}

//a function to convert the maximum amount of points into a %
function convertToPercentage(points) {
	var converter = 100 / maxPoints * points;
	converter = Math.round(converter);
	return converter;
}

//a quick functio to clear results from the ul
function clearResultsFromList() {
	console.log(partyScore);
	resultsList.innerHTML = "";
	for (var s = 0; s < parties.length; s++) {
	    partyScore[s] = {"name": parties[s].name, "points": 0};
	}
	filterPass = false;
	calculateResults();
	viewSwitch('results');
}

//a simple document reload to start over
function redo() {
	location = location;
}